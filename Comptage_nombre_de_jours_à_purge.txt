-- Comptage nombre de jours à purger

CREATE OR REPLACE FUNCTION nombre_de_jour(idAdherent INTEGER) RETURNS INTEGER AS $$
DECLARE
    nb_perdu INTEGER;
    retard_en_cours INTEGER;
    jours_reste_sanctions INTEGER;
BEGIN
    -- Appel de la fonction nombre_de_perdu
    nb_perdu := nombre_de_perdu(idAdherent);

    -- Si des prêts sont perdus, retourne -1
    IF nb_perdu > 0 THEN
        RETURN -1;
    END IF;

    -- Appel de la fonction retard_en_cours
    retard_en_cours := retard_en_cours(idAdherent);

    -- Si des retards en cours, retourne -1
    IF retard_en_cours > 0 THEN
        RETURN -1;
    END IF;

    -- Appel de la fonction dernier_retard
    jours_reste_sanctions := jours_reste_sanctions(idAdherent);

    -- Retourne le résultat de la fonction dernier_retard
    RETURN jours_reste_sanctions;

END;
$$ LANGUAGE plpgsql;

-- Fonction nombre_de_perdu
CREATE OR REPLACE FUNCTION nombre_de_perdu(idAdherent INTEGER) RETURNS INTEGER AS $$
DECLARE
    nb_perdu INTEGER;
BEGIN
    -- Compte le nombre de prêts 'perdu' pour l'adhérent
    SELECT COUNT(*)
    INTO nb_perdu
    FROM pret
    WHERE id_adherent = nombre_de_perdu.idAdherent
    AND etat = 'perdu';

    RETURN nb_perdu;
END;
$$ LANGUAGE plpgsql;

-- Fonction retard_en_cours
CREATE OR REPLACE FUNCTION retard_en_cours(idAdherent INTEGER) RETURNS INTEGER AS $$
DECLARE
    retard_en_cours INTEGER;
BEGIN
    -- Vérifie s'il y a un retard en cours pour l'adhérent
    SELECT COUNT(*)
    INTO retard_en_cours
    FROM pret
    WHERE id_adherent = retard_en_cours.idAdherent
    AND etat = 'en attente'
    AND (CURRENT_DATE - (date_debut + duree)) > 0;

    RETURN retard_en_cours;
END;
$$ LANGUAGE plpgsql;

-- Fonction dernier_retard
CREATE OR REPLACE FUNCTION jours_reste_sanctions(idAdherent INTEGER) RETURNS INTEGER AS $$
DECLARE
    dernier_retard INTEGER;
BEGIN
    -- Trouve le dernier prêt rendu par l'adhérent
    SELECT INTO dernier_retard
    date_rendu_effective + (date_rendu_effective - date_debut - duree) - current_date
    FROM pret
    WHERE id_adherent = jours_reste_sanctions.idAdherent
    AND date_rendu_effective IS NOT NULL
    ORDER BY date_rendu_effective DESC
    LIMIT 1;

    -- Si le retard est inférieur à 1 jour, retourne 0
    IF dernier_retard < 1 THEN
        RETURN 0;
    ELSE
	RETURN dernier_retard;
    END IF;
END;
$$ LANGUAGE plpgsql;


