@startuml
class Ressource {
  type : string
  code : int {key}
  titre : string
  dateApparition : date
  classification : int
}
class Exemplaire {
  id : int {key}
  état : {'Neuf', 'Abîmé', 'Bon', 'Perdu'},
  empruntable() : bit
}
class Editeur {
  id : int {key}
  nom : string
}
class Genre {
  id : int {key}
  nom : string
}
class Langue {
  id : int {key}
  nom : string
}
class Livre {
  ISBN : string
  resume : string
}
class OeuvreMusicale {
  longueur : int
}
class Film {
  longueur : int
  synopsis : string
}
class Pret {
  datePret : date
  dureePret : int
  adresse : string
  dateRendu : date
  dateRenduPrevue : date
  nb_jours_Retard : int
  état_retour : {'Remboursé', 'Perdu', 'En attente', 'Mm_état', 'Déterioré'}
}
class Personne {
  id : int {key}
  nom : string
  prenom : string
}
class Contributeur {
  date_nais : date
  nationalité : string
}
class Personnel {
  adresse : string
  adresse_mail :string
  login : string {UNIQUE}
  mdp : string

}
class Adhérent {
  adresse : string
  adresse_mail :string
  login : string {UNIQUE}
  mdp : string
  nb_jours_sanction() : int
  nb_sanctions() : int
  blackliste() : bit
}
  Personne <|-- Contributeur
  Personne <|-- Personnel
  Personne <|-- Adhérent
  Ressource <|-- Livre
  Ressource <|-- OeuvreMusicale
  Ressource <|-- Film
  Contributeur "1..*" - "1..*" Livre : est_Auteur
  Contributeur "1..*" - "1..*" Film : est_Réalisateur
  Contributeur "1..*" - "1..*" Film : est_Acteur
  Contributeur "1..*" - "1..*" OeuvreMusicale : est_Compositeur
  Contributeur "1..*" - "1..*" OeuvreMusicale : est_Interprète
  Adhérent "1..*" - "1..*" Exemplaire
  (Adhérent, Exemplaire) .. Pret
  note "XOR" as N1
  N1 .. Film
  N1 .. Livre
  N1 .. OeuvreMusicale
  Exemplaire "1..*" <-- "1" Ressource
  Ressource "1..*" <-- "1" Genre : appartient
  Ressource "1..*" <-- "1" Editeur : édite
  Film "1..*" <-- "1" Langue
  Livre "1..*" <-- "1" Langue
  note left of Adhérent::nb_jours_sanction
  nb_jours_sanction = -1 (donc l'adhérent ne peut pas effectuer d'emprunt), si l'adhérent a perdu un exmplaire qu'il a emprunté\nou si un de ses prêts est en retard
  end note
  note left of Adhérent::nb_sanctions
  nb_sanctions= En cas de perte ou détérioration grave d'un document, la suspension du droit de prêt est maintenue jusqu'à ce que l'adhérent rembourse le document. Enfin, la bibliothèque peut choisir de blacklister un adhérent en cas de sanctions répétées.
  end note
  note left of Adhérent::blackliste
  blackliste est updaté par le personnel\nsi il juge que l'adhérent doit être blacklisté. Il peut s'aider pour cela de l'appel de la fonction nb_sanctions
  end note
  note right of Exemplaire::empruntable
  un exemplaire ne peut être emprunté que si son état='Neuf' ou 'Bon' et s'il n'est pas dans la table Pret
  end note

@enduml
