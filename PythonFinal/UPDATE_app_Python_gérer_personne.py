import psycopg2
import prettytable
def gerer_personne(cursor):
	print("1. Gérer les adhérents")
	print("2. Gérer les contributeurs")
	print("3. Gérer le personnel")
	print("4. Quitter")
	choix = input("Choisissez une option : ")
	if choix == '1':
		quitter=False
		while not quitter :
			# Gérer les adhérents
			print("1. Ajouter un adhérent")
		    print("2. Modifier les informations d'un adhérent")
			print("3. Supprimer un adhérent")
			print("4. Retour")
		    choix_Action =str(input("Choisissez une option : "))
		    if choix_Action == '1':
				# Ajouter un adhérent
				choix_Personne =str(input("L'adhérent est-il déjà membre du personnel, ou un contributeur d'une ressource? (1-Oui, 2-Non)"))
				if choix_Personne == '1':
					# Si l'adhérent existe déjà dans la base, on demande son id
					id_Personne=str(input("Entrez l'id de la personne :"))
		            ajouter_Adherent_ou_Personnel(cursor, "Adherent", id_Personne)
				elif choix_Personne == '2':
					# Si l'adhérent est une nouvelle personne dans la base,
					# L'id de la nouvelle personne est généré automatiquement et on appelle ajouter_Personne puis ajouter_Adherent
					id=ajouter_Personne(cursor)
					if id :
						ajouter_Adherent_ou_Personnel(cursor, "Personnel", id)
					else :
						print("Ajout non effectué")


	    	elif choix_Action == '2':
				# Modifier les infos d'un adhérent
		        id_Personne=int(input("Entrez l'id de la personne :"))
		        # On affiche les données de l'adhérent
		        print(f"Voici les informations de l'adhérent' '{id_Personne}'")
		        afficher_Adherent_ou_Personnel(cursor, "Adherent", id_Personne)
				modifier_Adherent_ou_Personnel(cursor, id_Personne, "Adherent")

			elif choix_Action == '3':
				personne_supprimee=False
				while not personne_supprimee :
					# Supprimer un adhérent
		        	id_Personne=int(input("Entrez l'id de la personne :"))
		        	# On affiche les données de la personne
		        	print(f"Voici les informations de la personne {id_Personne}")
		        	ok=afficher_Personne(cursor, id_Personne)
					if not ok :
						print("Id invalide.")
						break
					bonne_personne=int(input("Attention ! Est-ce la bonne personne ? (1-Oui, 0-Non)"))
					if bonne_personne :
						requete=f"DELETE FROM Personne WHERE id_Personne = {id_Personne};"
					    	try:
						        cursor.execute(requete)
							except psycopg2.Error as e:
								# Gérer les erreurs psycopg2
								print("Erreur en faisant la requete suivante : ", e)
								ok=False
							ok=True
						personne_supprimee=ok
					else :
						print("Vérifiez l'id.")

			elif choix_Action == '4':
				# Quitter
				retour=True
	    	else :
	        	print("Attention, choisissez 1, 2, 3 ou 4!")

	elif choix == '3':
		quitter=False
		while not quitter :
			# Gérer le personnel
			print("1. Ajouter un membre du personnel")
		    print("2. Modifier les informations d'un membre du personnel")
			print("3. Supprimer un membre du personnel")
			print("4. Retour")
		    choix_Action = input("Choisissez une option : ")
		    if choix_Action == '1':
				# Ajouter un adhérent
				choix_Personne = int(input("Le membre est-il déjà un adhérent, ou un contributeur d'une ressource? (1-Oui, 0-Non)"))
				if choix_Personne :
					# Si l'adhérent existe déjà dans la base, on demande son id
					id_Personne=str(input("Entrez l'id de la personne :"))
		            ajouter_Adherent_ou_Personne(cursor, "Personnel", id_Personne)
				else :
					# Si l'adhérent est une nouvelle personne dans la base,
					# On appelle ajouter_Personne puis ajouter_Personel
					id=ajouter_Personne(cursor)
					if id :
						ajouter_Adherent_ou_Personnel(cursor, "Personnel", id)
					else :
						print("Ajout non effectué")

	    	elif choix_Action == '2':
				# Modifier les infos d'un adhérent
				id_Personne=int(input("Entrez l'id de la personne :"))
				# On affiche les données de l'adhérent
				print(f"Voici les informations de la personne' '{id_Personne}'")
				afficher_Adherent_ou_Personnel(cursor, "Personnel", id_Personne)
				modifier_Adherent_ou_Personnel(cursor, id_Personne, "Personnel")

			elif choix_Action == '3':
				personne_supprimee=False
				while not personne_supprimee :
					# Supprimer un adhérent
		        	id_Personne=int(input("Entrez l'id de la personne :"))
		        	# On affiche les données de la personne
		        	print(f"Voici les informations de la personne {id_Personne}")
		        	ok=afficher_Personne(cursor, id_Personne)
					if not ok :
						print("Id invalide.")
						break
					bonne_personne=int(input("Attention ! Est-ce la bonne personne ? (1-Oui, 0-Non)"))
					if bonne_personne :
					    sql=f"DELETE FROM Personne WHERE id_Personne = {id_Personne};"
					    	try:
						        cursor.execute(requete)
							except psycopg2.Error as e:
								# Gérer les erreurs psycopg2
								print("Erreur en faisant la requete suivante : ", e)
								ok=False
							ok=True
						personne_supprimee=ok
					else :
						print("Vérifiez l'id.")

			elif choix_Action == '4':
				# retour
				quitter=True
	    	else :
	        	print("Attention, choisissez 1, 2, 3 ou 4!")

	elif choix == '2':
		quitter=False
		while not quitter :
			# Gérer les contributeurs
			print("1. Ajouter un contributeur")
			print("2. Lier un contributeur à une ressource")
		    print("3. Modifier les informations d'un contributeur")
			print("4. Supprimer une contribution d'un contributeur")
			print("5. Supprimer un contributeur")
			print("6. Retour")
		    choix_Action = input("Choisissez une option : ")
		    if choix_Action == '1':
				# Ajouter un adhérent
				choix_Personne = int(input("Le membre est-il déjà un adhérent, ou un membre du personnel (1-Oui, 0-Non)"))
				if choix_Personne :
					# Si l'adhérent existe déjà dans la base, on demande son id
					id_Personne=str(input("Entrez l'id de la personne :"))
		            ajouter_Contributeur(cursor, id_Personne)
				else :
					# Si l'adhérent est une nouvelle personne dans la base,
					# On appelle ajouter_Personne puis ajouter_Personel
					id=ajouter_Personne(cursor)
					if id :
						ajouter_Contributeur(cursor, id)
					else :
						print("Ajout non effectué")

			elif choix_Action == '2':
				# On affiche les données du contributeur
				id_Personne=int(input("Entrez l'id de la personne :"))
				print(f"Voici les contributions de la personne '{id_Personne}'")
				afficher_contributions_Contributeur(cursor, id_Personne)

				# Lier un contributeur à une ressource
				type_Ressource=int(input("Entrez le type de la ressource à laquelle on ajoute un contributeur :"))
				ajouter_Contribution(cursor, type_Ressource, id_Personne)

	    	elif choix_Action == '3':
				# Modifier les infos d'un contributeur
				id_Personne=int(input("Entrez l'id de la personne :"))
				# On affiche les données du contributeur
				print(f"Voici les informations de la personne '{id_Personne}'")
				if afficher_Contributeur(cursor, id_Personne):
					modifier_Contributeur(cursor, id_Personne, "Personnel")

			elif choix_Action == '4':
				# Supprimer une contribution d'un contributeur
				id_Personne=int(input("Entrez l'id de la personne :"))
				print(f"Voici les contributions de la personne '{id_Personne}'")
				afficher_contributions_Contributeur(cursor, id_Personne)

				# Lier un contributeur à une ressource
				type_Ressource=int(input("Entrez le type de la ressource dont on souhaite supprimer la contribution :"))
				supprimer_Contribution(cursor, type, id_contributeur):

			elif choix_Action == '5':
				personne_supprimee=False
				while not personne_supprimee :
					# Supprimer un contributeur
		        	id_Personne=int(input("Entrez l'id de la personne :"))
		        	# On affiche les données de la personne
		        	print(f"Voici les informations de la personne {id_Personne}")
		        	ok=afficher_Personne(cursor, id_Personne)
					if not ok :
						print("Id invalide.")
						break
					bonne_personne=int(input("Attention ! Est-ce la bonne personne ? (1-Oui, 0-Non)"))
					if bonne_personne :
					    sql=f"DELETE FROM Personne WHERE id_Personne = {id_Personne};"
					    	try:
						        cursor.execute(requete)
							except psycopg2.Error as e:
								# Gérer les erreurs psycopg2
								print("Erreur en faisant la requete suivante : ", e)
								ok=False
							ok=True
						personne_supprimee=ok
					else :
						print("Vérifiez l'id.")

			elif choix_Action == '6':
				# Quitter
				quitter=True
	    	else :
	        	print("Attention, choisissez 1, 2, 3, 4, 5 ou 6!")

		elif choix == '4':
			# Quitter
			return
		else :
			print("Attention, choisissez 1, 2, 3 ou 4!")



def ajouter_Adherent_ou_Personnel(cursor, type, id_Personne):
	# On renseigne l'adresse de la personne, son adresse mail, son login, et son mot de passe
	adresse=str(input("Entrez l'adresse de la personne:"))
	adresse_mail=str(input("Entrez l'adresse mail de la personne:"))
	login=str(input("Entrez le login de la personne:"))
	mdp=str(input("Entrez le mot de passe de la personne:"))
	requete = f"INSERT INTO {type} VALUES ('{id_Personne}', '{adresse}', '{adresse_mail}', '{login}', '{mdp}');"
	try:
        cursor.execute(requete)
	except psycopg2.Error as e:
		# Gérer les erreurs psycopg2
		print("Erreur en faisant la requete suivante : ", e)
		return False
	return True

def ajouter_Contributeur(cursor, id_Personne):
	# On renseigne l'adresse de la personne, son adresse mail, son login, et son mot de passe
	date_nais=str(input("Entrez la date de naissance du contributeur:"))
	nationalite=str(input("Entrez la nationalité du contributeur:"))
	requete = f"INSERT INTO Contributeur VALUES ('{id_Personne}', '{date_nais}', '{nationalité}');"
	try:
        cursor.execute(requete)
	except psycopg2.Error as e:
		# Gérer les erreurs psycopg2
		print("Erreur en faisant la requete suivante : ", e)
		return False
	return True

def ajouter_Contribution(cursor, type, id_contributeur):
    code_ressource = input("Entrez le code de la ressource : ")

    query = f"INSERT INTO est_{type} (id_Personne, code_Ressource) VALUES ('{id_contributeur}', {code_ressource});"

    cursor.execute(query, values)
    print("Auteur ajouté avec succès.")

def ajouter_Personne(cursor):
	# On renseigne le nom et le prénom de la personne
	nom=str(input("Entrez le nom de la personne:"))
	return
	prenom=str(input("Entrez le prénom de la personne:"))
	return
	requete = f"INSERT INTO Personne VALUES ('{nom}', '{prenom}');"
	try:
        cursor.execute(requete)
	except psycopg2.Error as e:
		# Gérer les erreurs psycopg2
		print("Erreur en faisant la requete suivante : ", e)
		return False
	query= f"SELECT MAX(id_Personne) FROM Personne;"
	try:
		cursor.execute(query_genres)

		# Récupérer la première ligne du résultat
		result = cursor.fetchone()
		id=result[0]

		return id
	except psycopg2.Error as e:
		# Gérer les erreurs psycopg2
		print("Erreur en faisant la requete suivante : ", e)
	return False
	# On renvoie False si une erreur est apparue

def modifier_Adherent_ou_Personnel(cursor, id, type):
	pas_choix=True
	while pas_choix :
		choix_Modif = input("Quelle information souhaitez-vous modifier ? (adresse, adresse_mail, login, blackliste) ")
		if choix_Modif == 'adresse':
			# Modifier l'adresse
			pas_choix=False
			adresse=str(input("Nouvelle adresse : "))
			requete=(f"UPDATE {type} SET adresse = '{adresse}' WHERE id_Personne = {id};")
			try:
				cursor.execute(requete)
			except psycopg2.Error as e:
				# Gérer les erreurs psycopg2
				print("Erreur en faisant la requete suivante : ", e)
		elif choix_Modif == 'adresse_mail':
			# Modifier l'adresse mail
			pas_choix=False
			adresse_mail=str(input("Nouvelle adresse mail : "))
			requete=(f"UPDATE {type} SET adresse_mail = '{adresse_mail}' WHERE id_Personne = {id};")
			try:
				cursor.execute(requete)
			except psycopg2.Error as e:
				# Gérer les erreurs psycopg2
				print("Erreur en faisant la requete suivante : ", e)
		elif choix_Modif == 'login':
			# Modifier le login
			pas_choix=False
			login=str(input("Nouveau login : "))
			requete=(f"UPDATE {type} SET login = '{login}' WHERE id_Personne = {id};")
			try:
				cursor.execute(requete)
			except psycopg2.Error as e:
				# Gérer les erreurs psycopg2
				print("Erreur en faisant la requete suivante : ", e)
		elif choix_Modif == 'blackliste':
			# Modifier le login
			pas_choix=False
			affiche_sanctions(cursor, id)
			login=str(input("Nouvel etat de blackliste : "))
			requete=(f"UPDATE {type} SET login = '{login}' WHERE id_Personne = {id};")
			try:
				cursor.execute(requete)
			except psycopg2.Error as e:
				# Gérer les erreurs psycopg2
				print("Erreur en faisant la requete suivante : ", e)
		else :
			print("Attention, entrez un type de donnée valide!")

def modifier_Contributeur(cursor, id):
	pas_choix=True
	while pas_choix :
		choix_Modif = input("Quelle information souhaitez-vous modifier ? (date_nais, nationalité) ")
		if choix_Modif == 'date_nais':
			# Modifier la date de naissance
			pas_choix=False
			date_nais=str(input("Nouvelle date de naissance : "))
			requete=(f"UPDATE Contributeur SET date_nais = '{date_nais}' WHERE id_Personne = {id};")
			try:
				cursor.execute(requete)
			except psycopg2.Error as e:
				# Gérer les erreurs psycopg2
				print("Erreur en faisant la requete suivante : ", e)
		elif choix_Modif == 'nationalité':
			# Modifier la nationalité
			pas_choix=False
			nationalité=str(input("Nouvelle nationalité : "))
			requete=(f"UPDATE Contributeur SET nationalite = '{nationalité}' WHERE id_Personne = {id};")
			try:
				cursor.execute(requete)
			except psycopg2.Error as e:
				# Gérer les erreurs psycopg2
				print("Erreur en faisant la requete suivante : ", e)
		else :
			print("Attention, entrez un type de donnée valide!")

def supprimer_Contribution(cursor, type, id_contributeur):
    code_ressource = input("Entrez le code de la ressource : ")

    query = f"DELETE FROM est_{type} WHERE id_Personne = {id_contributeur} AND code_Ressource = {code_ressource};"

    cursor.execute(query)
    print(f"Contribution de type {type} supprimée avec succès.")

def afficher_Personne(cursor, id_Personne):
	# Affiche les infos d'une personne, renvoie Vrai si pas d'erreurs
    query = f"SELECT nom AS 'Nom', prenom 'Prenom' FROM Personne WHERE id = {id_Personne};"
    try:
        cursor.execute(query)

        # Récupérer la première ligne du résultat
        result = cursor.fetchone()

        # Si aucun genre existe
        if result is None:
            return False
        else:
            # Créer une instance de PrettyTable pour l'exemplaire
            affichage = prettytable.PrettyTable()

            # Ajouter les colonnes et les données à la table en évitant les valeurs None
            for key, value in zip(cursor.description, result):
                if value is not None:
                    affichage.add_column(key[0], [value])

            # Afficher la Personne
			print(affichage)

    except psycopg2.Error as e:
        # Gérer les erreurs psycopg2
        print("Erreur en faisant la requete suivante : ", e)
        return False
	return True

def afficher_Adherent_ou_Personnel(cursor, type, id_Personne):
	# Affiche les infos d'un adherent, renvoie Vrai si pas d'erreurs
	if type=="Adherent":
		bl=", blackliste as 'Est Blackliste'"
	else :
		bl=""
    query = f"SELECT adresse AS 'Adresse', adresse_mail AS 'Adresse Mail', login AS 'Login'{bl} FROM {type} WHERE id = {id_Personne};"
    try:
        cursor.execute(query)

        # Récupérer la première ligne du résultat
        result = cursor.fetchone()

        # Si aucun genre existe
        if result is None:
            return False
        else:
            # Créer une instance de PrettyTable pour l'exemplaire
            affichage = prettytable.PrettyTable()

            # Ajouter les colonnes et les données à la table en évitant les valeurs None
            for key, value in zip(cursor.description, result):
                if value is not None:
                    affichage.add_column(key[0], [value])

            # Afficher la Personne
			print(affichage)

    except psycopg2.Error as e:
        # Gérer les erreurs psycopg2
        print("Erreur en faisant la requete suivante : ", e)
        return False
	return True

def afficher_Contributeur(cursor, id_Personne):
    query = f"SELECT date_nais AS 'Date de naissance', nationalite AS 'Nationalité' FROM Contributeur WHERE id = {id_Personne};"
    try:
        cursor.execute(query)

        # Récupérer la première ligne du résultat
        result = cursor.fetchone()

        # Si aucun genre existe
        if result is None:
            return False
        else:
            # Créer une instance de PrettyTable pour l'exemplaire
            affichage = prettytable.PrettyTable()

            # Ajouter les colonnes et les données à la table en évitant les valeurs None
            for key, value in zip(cursor.description, result):
                if value is not None:
                    affichage.add_column(key[0], [value])

            # Afficher la Personne
			print(affichage)

    except psycopg2.Error as e:
        # Gérer les erreurs psycopg2
        print("Erreur en faisant la requete suivante : ", e)
        return False
	return True

def affiche_contributions_Contributeur(cursor, id_Contributeur):
    query = """
    SELECT DISTINCT R.titre, R.dateApparition, R.classification
    FROM Ressource R
    LEFT JOIN est_Auteur A ON R.code = A.code_Ressource
    LEFT JOIN est_Acteur AC ON R.code = AC.code_Ressource
    LEFT JOIN est_Interprete I ON R.code = I.code_Ressource
    LEFT JOIN est_Compositeur C ON R.code = C.code_Ressource
    LEFT JOIN est_Realisateur RE ON R.code = RE.code_Ressource
    WHERE A.id_Personne = %s OR AC.id_Personne = %s OR I.id_Personne = %s OR C.id_Personne = %s OR RE.id_Personne = %s;
    """

    values = (id_Contributeur, id_Contributeur, id_Contributeur, id_Contributeur, id_Contributeur)

    cursor.execute(query, values)
    contributions = cursor.fetchall()

    if contributions:
        print("\nContributions du contributeur (Auteur, Acteur, Interprète, Compositeur, Réalisateur):")
        for contribution in contributions:
            print(f"Titre: {contribution[0]}, Date d'apparition: {contribution[1]}, Classification: {contribution[2]}")
    else:
        print("Aucune contribution trouvée pour ce contributeur.")


def affiche_sanctions(cursor, id_Personne):
	print(f"Infos de sanctions pour l'adherent {id_Personne}")
	requete=(f"SELECT nombre_de_jour(id_Personne), nombre_de_sanctions(id_Personne);")
	try:
		cursor.execute(query)
		# Récupérer la première ligne du résultat
		result = cursor.fetchone()
		# Si pas bon id
		if result is None:
			print("Id non trouvé.")
			return False
		else:
			# Créer une instance de PrettyTable pour l'exemplaire
			affichage = prettytable.PrettyTable()
			# Ajouter les colonnes et les données à la table en évitant les valeurs None
			for key, value in zip(cursor.description, result):
				if value is not None:
					affichage.add_column(key[0], [value])

			# Afficher les sanctions
			print(affichage)

	except psycopg2.Error as e:
		# Gérer les erreurs psycopg2
		print("Erreur en faisant la requete suivante : ", e)
		return False
	return True
