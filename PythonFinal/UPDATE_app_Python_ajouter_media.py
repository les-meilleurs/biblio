import re  # module pour l'expression régulière
import psycopg2
import prettytable

def ajouter_media(cursor):
    quitter=False
    while not quitter :
        date_ok=False
        print("1. Ajouter une ressource")
        print("2. Ajouter un exemplaire d'une ressource")
        print("3. Quitter")
        choix = input("Choisissez une option : ")
        if choix == '1':
            # Ajouter une ressource
            # On renseigne le titre de la ressource, sa date d'apparition et sa classification.
            titre=str(input("Entrez le titre de la ressource :"))
            while not date_ok:
                date_apparition = input("Entrez la date d'apparition de la ressource : ")
                # Utilisez votre fonction de vérification de date ici
                date_ok = verifier_date(date_apparition)
                if not date_ok:
                    print("Date invalide. Veuillez réessayer avec le format 'YYYY-MM-DD'")
            classification=int(input("Entrez la classification de la ressource :"))

            # On affiche les listes des codes des éditeurs, des langues et des genres qui sont déjà dans la base avant de les renseigner
            afficher_editeurs(cursor)
            nouveau=int(input("L'éditeur est-il dans la liste? (1-Non, 0-Oui)"))
            if nouveau :
                nom=str(input("Entrez le nom de l'éditeur de la ressource :"))
                editeur=creer_editeur(cursor, nom)
            else :
                editeur=int(input("Entrez l'id de l'éditeur de la ressource :"))

            afficher_genres(cursor)
            nouveau=int(input("Le genre est-il dans la liste? (1-Non, 0-Oui)"))
            if nouveau :
                nom=str(input("Entrez le nom du genre de la ressource :"))
                genre=creer_genre(cursor, nom)
            else :
                genre=int(input("Entrez l'id du genre de la ressource :"))

            afficher_langues(cursor)
            nouveau=int(input("Le genre est-il dans la liste? (1-Non, 0-Oui)"))
            if nouveau :
                nom=str(input("Entrez le nom de la langue de la ressource :"))
                langue=creer_langue(cursor, nom)
            else :
                langue=int(input("Entrez l'id du genre de la ressource :"))

            if not editeur or genre or langue:
                print("Ajout non effectué")
            else :
                requete = f"INSERT INTO Ressource VALUES ('{titre}', TO_DATE('{date_apparition}','YYYY-MM-DD'), {classification}, {editeur}, {genre}, {langue});"
    	        try:
                    cursor.execute(requete)
    	        except psycopg2.Error as e:
                    # Gérer les erreurs psycopg2
    		        print("Erreur en faisant la requete suivante : ", e)

                # On donne le choix d'ajouter un exemplaire pour cette ressource
                choix_Ex = input("Souhaitez-vous ajouter un exemplaire pour cette ressource ? (1-Oui, 2-Non)")
                if choix_Ex == '1':
                    # Requête SQL pour récupérer le code de la ressource
                    query= f"SELECT MAX(id) FROM Ressource;"
                    try:
                        cursor.execute(query)
                        # Récupérer la première ligne du résultat
                        result = cursor.fetchone()
                        code_ressource=result[0]
                        ajouter_exemplaire(cursor, code_ressource)
                    except psycopg2.Error as e:
                        # Gérer les erreurs psycopg2
                        print("Erreur en faisant la requete suivante : ", e)
                    break
                elif choix_Ex == '2':
                    choix_Res = input("OK, souhaitez-vous ajouter une autre ressource ? (1-Oui, 2-Non)")
                    if choix_Res == '1':
                        choix = '1'
                    else choix_Res == '2':
                        break
                else :
                    print("Attention, choisissez 1 ou 2!")

        if choix == '2':
            # Ajouter un exemplaire d'une ressource
            code_Ressource=int(input("Entrez le code de la ressource : "))

            # On affiche les données de la ressource
            print(f"Voici les informations de la ressource '{code_Ressource}'")
            if recherche_ressource(cursor, code_Ressource) :
                ajouter_exemplaire(cursor, code_Ressource)

        if choix == '3':
            # Quitter
            quitter = True

        else :
            print("Attention, choisissez 1, 2 ou 3!")

def verifier_date(date):
    # Utilisation d'une expression régulière pour vérifier si la date a un format valide
    pattern = re.compile(r'^\d{4}-\d{2}-\d{2}$')

    return bool(pattern.match(date))

def ajouter_exemplaire(cursor, code_Ressource):
    print("Quel est l'état de l'exemplaire ?")
    ok=0
    while not ok :
        etat=str(input("Choisissez parmis Neuf, Abîmé, et Bon:"))
        if etat in ('Neuf', 'Abîmé', 'Bon') :
            ok=1
    # On a toutes les données nécessaires à l'ajout, donc on peut faire appel à la requête
	try:
        requete = f"INSERT INTO Exemplaire VALUES ('{etat}', {code_Ressource}, 1);"
        cursor.execute(requete)
	except psycopg2.Error as e:
        # Gérer les erreurs psycopg2
		print("Erreur en faisant la requete suivante : ", e)
    return

def afficher_editeurs(cursor) :
    # Requête SQL pour récupérer les éditeurs
    query_editeurs = f"SELECT * FROM Editeur;"
    try:
        cursor.execute(query_editeurs)

        # Récupérer la première ligne du résultat
        result_editeurs = cursor.fetchone()

        # Si aucun éditeur existe
        if result_editeurs is None:
            print("Aucun editeur n'est enregistré.")
        else:
            # Créer une instance de PrettyTable pour l'exemplaire
            table_editeurs = prettytable.PrettyTable()

            # Ajouter les colonnes et les données à la table en évitant les valeurs None
            for key, value in zip(cursor.description, result_editeurs):
                if value is not None:
                    table_editeurs.add_column(key[0], [value])

            # Afficher la table de l'exemplaire
            print("Editeurs enregistrés :")
            print(table_editeurs)

        return

    except psycopg2.Error as e:
        # Gérer les erreurs psycopg2
        print("Erreur en faisant la requete suivante : ", e)
        return

def afficher_genres(cursor) :
    # Requête SQL pour récupérer les genres
    query_genres = f"SELECT * FROM Genre;"
    try:
        # Exécuter la première requête pour obtenir les informations de l'exemplaire et son état
        cursor.execute(query_genres)

        # Récupérer la première ligne du résultat
        result_genres = cursor.fetchone()

        # Si aucun genre existe
        if result_genres is None:
            print("Aucun genre n'est enregistré.")
        else:
            # Créer une instance de PrettyTable pour l'exemplaire
            table_genres = prettytable.PrettyTable()

            # Ajouter les colonnes et les données à la table en évitant les valeurs None
            for key, value in zip(cursor.description, result_genres):
                if value is not None:
                    table_genres.add_column(key[0], [value])

            # Afficher la table de l'exemplaire
            print("Genres enregistrés :")
            print(table_genres)

        return

def afficher_langues(cursor) :
    # Requête SQL pour récupérer les genres
    query_langues = f"SELECT * FROM Langue;"
    try:
        # Exécuter la première requête pour obtenir les informations de l'exemplaire et son état
        cursor.execute(query_langues)

        # Récupérer la première ligne du résultat
        result_langues = cursor.fetchone()

        # Si aucun genre existe
        if result_langues is None:
            print("Aucune langue n'est enregistré.")
        else:
            # Créer une instance de PrettyTable pour l'exemplaire
            table_langues = prettytable.PrettyTable()

            # Ajouter les colonnes et les données à la table en évitant les valeurs None
            for key, value in zip(cursor.description, result_langues):
                if value is not None:
                    table_langues.add_column(key[0], [value])

            # Afficher la table de l'exemplaire
            print("Langues enregistrés :")
            print(table_langues)

        return



def recherche_ressource(cursor, code_Ressource) :
    # Requête SQL pour récupérer les infos de la ressource
    query_res = f"SELECT titre AS 'Titre de la ressource', dateApparition AS 'Date d'apparition', classification AS 'Classification' FROM Ressource WHERE Ressource.code = {code_Ressource};"

    try:
        # Exécuter la première requête pour obtenir les informations de l'exemplaire et son état
        cursor.execute(query_res)

        # Récupérer la première ligne du résultat
        result_res = cursor.fetchone()

        # Si l'exemplaire n'existe pas
        if result_res is None:
            print("Le code que vous avez demandé n'existe pas.")
            return False
        else:
            # Créer une instance de PrettyTable pour l'exemplaire
            table_res = prettytable.PrettyTable()

            # Ajouter les colonnes et les données à la table en évitant les valeurs None
            for key, value in zip(cursor.description, result_res):
                if value is not None:
                    table_res.add_column(key[0], [value])

            # Afficher la table de l'exemplaire
            print("Informations de la ressource :")
            print(table_res)

            return True

    except psycopg2.Error as e:
        # Gérer les erreurs psycopg2
        print("Erreur en faisant la requete suivante : ", e)
        return False


def creer_editeur(cursor, editeur) :
    requete = f"INSERT INTO Editeur VALUES ('{editeur}');"
    try:
        cursor.execute(requete)
        query= f"SELECT MAX(id) FROM Editeur;"
        try:
            cursor.execute(query)
            result = cursor.fetchone()
            id_editeur=result[0]
            return id_editeur
        except psycopg2.Error as e:
            # Gérer les erreurs psycopg2
            print("Erreur en faisant la requete suivante : ", e)
        return False
    except psycopg2.Error as e:
    	# Gérer les erreurs psycopg2
    	print("Erreur en faisant la requete suivante : ", e)
        return False

def creer_genre(cursor, genre) :
    requete = f"INSERT INTO Genre VALUES ('{genre}');"
    try:
        cursor.execute(requete)
        query= f"SELECT MAX(id) FROM Genre;"
        try:
            cursor.execute(query)
            result = cursor.fetchone()
            id_editeur=result[0]
            return id_editeur
        except psycopg2.Error as e:
            # Gérer les erreurs psycopg2
            print("Erreur en faisant la requete suivante : ", e)
        return False
    except psycopg2.Error as e:
    	# Gérer les erreurs psycopg2
    	print("Erreur en faisant la requete suivante : ", e)
        return False

def creer_langue(cursor, langue) :
    requete = f"INSERT INTO Langue VALUES ('{langue}');"
    try:
        cursor.execute(requete)
        query= f"SELECT MAX(id) FROM Langue;"
        try:
            cursor.execute(query)
            result = cursor.fetchone()
            id_editeur=result[0]
            return id_editeur
        except psycopg2.Error as e:
            # Gérer les erreurs psycopg2
            print("Erreur en faisant la requete suivante : ", e)
        return False
    except psycopg2.Error as e:
    	# Gérer les erreurs psycopg2
    	print("Erreur en faisant la requete suivante : ", e)
        return False
