# -*- coding: utf-8 -*-

# Importation des fonctions
from connectionBDD import connect_to_database, close_connection
from InfoExemplaire import recherche
from AfficherTop3 import top3
from UPDATE_app_Python_ajouter_media import ajouter_media
from Emprunter import emprunter
from Rendre import rendre
from ModifierPret import update_pret
from UPDATE_app_Python_gerer_personne import gerer_personne


def main():

    connection, curseur = connect_to_database()

    boucle = True

    if connection == None or curseur == None:
        boucle = False

    while boucle:
        print("\nMenu Principal:\n")
        print("1. Emprunter un exemplaire")
        print("2. Rendre un prêt")
        print("3. Modifier les informations d'un prêt")
        print("4. Afficher les informations d'un exemplaire")
        print("5. Ajouter une ressource, ou un de ses exemplaires")
        print("6. Afficher les informations d'une personne (adhérent ou personnel)")
        print("7. Afficher les 3 ressources les plus empruntées par catégorie")
        print("8. Quitter")
        print("\n\n")

        choix = input("Votre choix : ")

        if choix == '1':
            emprunter(curseur)
        elif choix == '2':
            rendre(curseur)
        elif choix == '3':
            update_pret(curseur)
        elif choix == '4':
            recherche(curseur)
        elif choix == '5':
            ajouter_media(curseur)
        elif choix == '6':
            gerer_personne(curseur)
        elif choix == '7':
            top3(curseur)
        elif choix == '8':
            boucle = False
        else:
            print("Option invalide. Veuillez choisir une option valide.")

    close_connection(connection, curseur)

if __name__ == "__main__":
    main()
