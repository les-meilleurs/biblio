import psycopg2
   
def top3(cursor):
    cursor.execute("""
        SELECT r.titre, COUNT(*) as borrow_count
        FROM Pret p
        JOIN Exemplaire e ON p.id_Exemplaire = e.id
        JOIN Ressource r ON e.code_Ressource = r.code
        JOIN Livre l ON r.code = l.code_Ressource
        GROUP BY r.titre
        ORDER BY borrow_count DESC
        LIMIT 1
    """)
    most_borrowed_livre = cursor.fetchone()

    cursor.execute("""
        SELECT r.titre, COUNT(*) as borrow_count
        FROM Pret p
        JOIN Exemplaire e ON p.id_Exemplaire = e.id
        JOIN Ressource r ON e.code_Ressource = r.code
        JOIN Film f ON r.code = f.code_Ressource
        GROUP BY r.titre
        ORDER BY borrow_count DESC
        LIMIT 1
    """)
    most_borrowed_film = cursor.fetchone()

    cursor.execute("""
        SELECT r.titre, COUNT(*) as borrow_count
        FROM Pret p
        JOIN Exemplaire e ON p.id_Exemplaire = e.id
        JOIN Ressource r ON e.code_Ressource = r.code
        JOIN OeuvreMusicale o ON r.code = o.code_Ressource
        GROUP BY r.titre
        ORDER BY borrow_count DESC
        LIMIT 1
    """)
    most_borrowed_oeuvre_musicale = cursor.fetchone()

    print("Livre le plus emprunté : %s, Film le plus emprunté : %s, Oeuvre musicale la plus empruntée : %s", most_borrowed_livre, most_borrowed_film, most_borrowed_oeuvre_musicale)
