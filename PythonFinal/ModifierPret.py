import psycopg2
from app_Python_ajouter_media import verifier_date

def display_menu():
    print("--- Mettre à jour le champ du prêt : ---")
    print("1. Date de prêt")
    print("2. Date de rendu")
    print("3. Nombre de jours de retard")
    print("4. État de retour")
    print("0. Fin")

def update_pret(cursor):

    pret_id = input("Entrez l'ID de l'exemplaire à mettre à jour: ")
    adherent_id = input("Entrez l'ID de l'adhérent du prêt: ")
    cursor.execute(f"SELECT * FROM Pret WHERE id_Exemplaire = {pret_id} and Adherent = {adherent_id} ")
    pret_record = cursor.fetchone()

    if pret_record:
        # Affichage des données de la ligne
        print("Données du prêt:")
        print("ID Exemplaire:", pret_record[0])
        print("ID Adhérent:", pret_record[1])
        print("Date du prêt:", pret_record[2])
        print("Durée du prêt:", pret_record[3])
        print("Date de rendu:", pret_record[4])
        print("Date de rendu prévue:", pret_record[5])
        print("Nombre de jours de retard:", pret_record[6])
        print("État du retour:", pret_record[7])
    else:
        print("Aucun prêt trouvé pour les ID spécifiés.")
   
    while True:
        display_menu()
        choice = input("Entrez votre choix: ")

        if choice == "0":
            break
        elif choice == "1":
            new_date_pret = input("Entrez la nouvelle date de prêt: ")
            if (verifier_date(new_date_pret)):
                cursor.execute(f"UPDATE Pret SET datePret = {new_date_pret} WHERE id_Exemplaire = {pret_id} and Adherent = {adherent_id} ")
                print("La date de prêt a été mise à jour avec succès.")
        elif choice == "2":
            new_date_rendu_prevue = input("Entrez la nouvelle date de rendu prévue: ")
            if (verifier_date(new_date_rendu_prevue)):
                cursor.execute(f"UPDATE Pret SET dateRenduPrevue = {new_date_rendu_prevue} WHERE id_Exemplaire = {pret_id} and Adherent = {adherent_id}")
                cursor.execute(f"UPDATE Pret SET dureePret = dureePret + DATEDIFF({new_date_rendu_prevue}, {date}) WHERE id_Exemplaire = {pret_id} and Adherent = {adherent_id};")
                print("La date de rendu prévue a été mise à jour avec succès.")
        elif choice == "3":
            new_nb_jours_retard = input("Entrez le nouveau nombre de jours de retard: ")
            if (verifier_date(new_nb_jours_retard)):
                cursor.execute(f"UPDATE Pret SET nb_jours_Retard = {new_nb_jours_retard} WHERE id_Exemplaire = {pret_id} and Adherent = {adherent_id}")
                print("Le nombre de jours de retard a été mis à jour avec succès.")
        elif choice == "4":
            new_etat_retour = input("Entrez le nouvel état de retour: ")
            cursor.execute(f"UPDATE Pret SET etat_retour = '{new_etat_retour}' WHERE id_Exemplaire = {pret_id} and Adherent = {adherent_id}")
            print("L'état de retour a été mis à jour avec succès.")
        else:
            print("Choix invalide.")
