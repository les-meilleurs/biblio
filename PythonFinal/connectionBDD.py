import psycopg2

def connect_to_database():
    try:
        # Établir la connexion
        connection = psycopg2.connect("dbname='dbai23a003' user='ai23a003' host='tuxa.sme.utc' password='EvaA3rHDe5dg'")

        # Créer un curseur pour exécuter des requêtes SQL
        cursor = connection.cursor()

        # Retourner la connexion et le curseur
        return connection, cursor

    except psycopg2.Error as e:
        print("Erreur lors de la connexion à la base de données:", e)
        return None, None

def close_connection(connection, cursor):
    # Fermer le curseur et la connexion
    if cursor:
        cursor.close()
    if connection:
        connection.close()
