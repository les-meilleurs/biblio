import psycopg2
import prettytable
from datetime import datetime, timedelta
from InfoExemplaire import recherche
from app_Python_gerer_personne import ajouter_Adherent_ou_Personnel

#fonction emprunter
def emprunter(cursor):
    """
    Gère le processus d'emprunt d'un exemplaire, incluant la recherche et la validation de l'emprunt.

    Parameters:
        cursor (psycopg2.extensions.cursor): Le curseur de la base de données.

    Returns:
        None
    """


    print("\nVous avez selectionne l'option Emprunter : ")

    # On appelle la fonction rechercher
    id_exemplaire = int(input("\nVeuillez entrer l'id de l'exemplaire à rechercher : "))
    recherche(cursor, id = id_exemplaire)

    confirmation = input("Voulez-vous emprunter cet exemplaire ? (Oui/Non): ").lower()#tout est mis en lowercase dans la question
    if confirmation != "oui":
        print("Emprunt annulé. Retour au menu.")
        return

    try:
        # Votre code SQL ici
        cursor.execute(f"SELECT empruntable FROM Exemplaire WHERE id = {id_exemplaire}")
        empruntable = cursor.fetchone()
        print(empruntable)

    except psycopg2.Error as e:
        conn.rollback()
        print(f"Erreur PostgreSQL : {e}")


    if not empruntable[0]:
        print("L'exemplaire n'est pas empruntable, retour menu")
        return

    id_adherent = int(input("Entrez l'ID de l'adhérent: "))
    if not verifier_adherent(cursor, id_adherent):
        choix = int(input("Adhérent inexistant :  1. Retour menu, 2. Ajouter l'adhérent : "))
        if choix != 2:
            print("retour au menu.")
            return
        else :
            print("Ajout de l'adherent :")
            ajouter_Adherent_ou_Personnel(cursor, "Adherent")
        return


    enregistrer_pret(cursor, id_exemplaire, id_adherent)
    print("Emprunt réalisé avec succès !")

#verifie l'existence de l'adherent
def verifier_adherent(cursor, id_adherent):
    cursor.execute("SELECT id_Personne FROM Adherent WHERE id_Personne = %s", (id_adherent,))
    return cursor.fetchone() is not None

    #enregistre le pret et determine la date de retour prevue
def enregistrer_pret(cursor, id_exemplaire, id_adherent):
    date_pret = datetime.now()
    duree_pret = 30  # Durée du prêt en jours à modifier
    try:
        date_rendu_prevue = date_pret + timedelta(days=duree_pret)
        cursor.execute("INSERT INTO Pret (datePret, dureePret, dateRenduPrevu, id_Exemplaire, Adherent) VALUES (%s, %s, %s, %s)", (date_pret, duree_pret, date_rendu_prevue, id_exemplaire, id_adherent))
        cursor.execute(f"UPDATE Exemplaire empruntable = false WHERE id = {id_exemplaire}")
        print(f"Bien emprunté, date de retour prévu : {date_rendu_prevue.strftime('%Y-%m-%d')}")
    except psycopg2.Error as e:
        print("Erreur lors de l'enregistrement du prêt:", e)
