# Analyse et Décisions pour la Modélisation de Base de Données

Après une analyse approfondie du sujet et une réflexion approfondie sur le modèle de données, notre équipe a pris des décisions cruciales pour affiner le modèle conformément aux consignes énoncées. Les choix stratégiques que nous avons opérés sont minutieusement détaillés ci-dessous.


## Classe "Personne" et Héritage

Nous avons créé la classe **Personne** avec un héritage non exclusif, instanciant ainsi les sous-classes *Contributeur*, *Membre*, et *Adhérent*. La clé de cette classe est un identifiant numérique, soigneusement généré pour assurer une identification unique des individus. L'héritage non exclusif permet à une personne d'assumer les rôles de contributeur, adhérent et membre simultanément. Par exemple, une personne pourrait être à la fois artiste contribuant à la bibliothèque et adhérente. Les personnes détenant les statuts de membre et d'adhérent se verront attribuer deux paires distinctes (login, mot de passe), facilitant ainsi la gestion des accès aux équipements informatiques de la bibliothèque.


## Gestion des Ressources

La clarté autour de la nature du code unique des ressources n'étant pas évidente, nous avons judicieusement conclu que le code unique de la ressource représente la ressource dans son ensemble. C'est pourquoi nous avons introduit la classe **Exemplaire**. Ainsi, deux livres représentant le même ouvrage partageront le même code, mais seront différenciés par un identifiant unique. L'état de l'exemplaire sera méticuleusement conservé dans la classe **Exemplaire**. Pour les films, livres et musiques, nous avons opté pour un héritage exclusif, car chaque ressource ne peut appartenir qu'à une seule catégorie à la fois.


## Gestion des Types de Contributeurs

Nous avons postulé qu'un contributeur peut appartenir à plusieurs types. Par exemple, un compositeur peut également être interprète, et un réalisateur peut être acteur. Ainsi, des classes d'association telles que "AssociationCompositeur" et "AssociationRealisateur" ont été créées. Concernant la cardinalité, nous avons décidé catégoriquement qu'un contributeur doit impérativement avoir contribué à une ressource de la bibliothèque, sous peine d'être supprimé. Il est crucial de planifier une suppression en cascade des deux côtés lors de la création de la base de données.


## Classes d'Énumération

Un choix judicieux supplémentaire a été l'introduction de trois classes d'énumération : "Langue" (composée des colonnes "id" et "langue"), "Editeur" (composée des colonnes "id" et "éditeur"), et "Genre" (composée des colonnes "id" et "genre"). Cette approche méticuleuse vise à éviter des erreurs de manipulation de la part des membres. En imposant une sélection à partir d'une liste prédéfinie, représentée par nos tables d'énumération, nous prévenons les doublons et assurons une standardisation rigoureuse des données.
