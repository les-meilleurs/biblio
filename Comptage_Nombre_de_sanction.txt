-- Comptage Nombre de sanction

CREATE OR REPLACE FUNCTION nombre_de_sanctions(idAdherent INTEGER) RETURNS INTEGER AS $$
DECLARE
    nb_sanctions INTEGER;
BEGIN
    -- Compte le nombre de prêts non rendus ou non en attente
    SELECT COUNT(*)
    INTO nb_sanctions
    FROM pret
    WHERE id_adherent = nombre_de_sanctions.idAdherent
        AND (
		(etat NOT IN ('rendu', 'en attente'))
		OR
        	(etat = 'en attente' AND (date_debut + duree) < CURRENT_DATE)
		OR
		(etat = 'rendu' AND (date_debut + duree) < date_rendu_effective)
	);
    -- Retourne le total des deux comptages
    RETURN nb_sanctions;
END;
$$ LANGUAGE plpgsql;