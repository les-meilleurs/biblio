import psycopg2
import prettytable

def recherche(cursor):
    """
    Demande un id d'exemplaire et recherche l'exemplaire dans la base de données pour donner les informations associées.

    Parameters:
        cursor (psycopg2.extensions.cursor): Le curseur de la base de données.

    Returns:
        None
    """
    id = int(input("Veuillez entrer l'id de l'exemplaire : "))

    # Requête SQL pour récupérer les informations de l'exemplaire et son état
    query_exemplaire = f"""
        SELECT
            E.etat AS "État de l'Exemplaire",
            R.titre AS "Titre de la Ressource",
            CASE
                WHEN R.code IN (SELECT code_Ressource FROM Livre) THEN 'Livre'
                WHEN R.code IN (SELECT code_Ressource FROM OeuvreMusicale) THEN 'OeuvreMusicale'
                WHEN R.code IN (SELECT code_Ressource FROM Film) THEN 'Film'
            END AS "Type de Ressource",
            L.ISBN AS "ISBN",
            OM.longueur AS "Longueur Oeuvre Musicale",
            F.longueur AS "Longueur Film"
        FROM
            Ressource R
        LEFT JOIN Livre L ON R.code = L.code_Ressource
        LEFT JOIN OeuvreMusicale OM ON R.code = OM.code_Ressource
        LEFT JOIN Film F ON R.code = F.code_Ressource
        JOIN Exemplaire E ON R.code = E.code_Ressource
        WHERE
            E.id = {id};
    """

    # Requête SQL pour récupérer les informations du dernier prêt de l'exemplaire
    query_dernier_pret = f"""
        SELECT
            P.datePret AS "Date du Prêt",
            P.dureePret AS "Durée du Prêt",
            P.dateRendu AS "Date de Rendu",
            P.nb_jours_Retard AS "Nombre de Jours de Retard",
            P.etat_retour AS "État du Retour"
        FROM
            Pret P
        WHERE
            P.id_Exemplaire = {id}
        ORDER BY
            P.dateRendu DESC
        LIMIT 1;
    """

    try:
        # Exécuter la première requête pour obtenir les informations de l'exemplaire et son état
        cursor.execute(query_exemplaire)

        # Récupérer la première ligne du résultat
        result_exemplaire = cursor.fetchone()

        # Si l'exemplaire n'existe pas
        if result_exemplaire is None:
            print("L'id que vous avez demandé n'existe pas.")
        else:
            # Créer une instance de PrettyTable pour l'exemplaire
            table_exemplaire = prettytable.PrettyTable()

            # Ajouter les colonnes et les données à la table en évitant les valeurs None
            for key, value in zip(cursor.description, result_exemplaire):
                if value is not None:
                    table_exemplaire.add_column(key[0], [value])

            # Afficher la table de l'exemplaire
            print("Informations de l'exemplaire :")
            print(table_exemplaire)

            # Exécuter la deuxième requête pour obtenir les informations du dernier prêt de l'exemplaire
            cursor.execute(query_dernier_pret)

            # Récupérer la première ligne du résultat
            result_dernier_pret = cursor.fetchone()

            # Si l'exemplaire n'a pas de prêt
            if result_dernier_pret is not None:
                # Créer une instance de PrettyTable pour le dernier prêt
                table_dernier_pret = prettytable.PrettyTable()

                # Ajouter les colonnes et les données à la table en évitant les valeurs None
                for key, value in zip(cursor.description, result_dernier_pret):
                    if value is not None:
                        table_dernier_pret.add_column(key[0], [value])

                # Afficher la table du dernier prêt
                print("\nInformations du dernier prêt de l'exemplaire :")
                print(table_dernier_pret)

        return

    except psycopg2.Error as e:
        # Gérer les erreurs psycopg2
        print("Erreur en faisant la requete suivante : ", e)
        return
