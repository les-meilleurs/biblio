import psycopg2
import prettytable
from datetime import datetime, timedelta

#Fonction Rendre
def rendre(cursor):
    recherche(cursor) 

    choix = input("Est-ce le bon exemplaire ? 1. Oui, 2. Retour menu: ")
    if choix != "1":
        print("Retour au menu principal.")
        return
    
    etat_actuel = cursor.execute(SELECT etat FROM Exemplaire)
    print("L'état actuel est %s", etat_actuel)

    etat_correct = input("L'état de l'exemplaire correspond-il à l'état attendu ? (Oui/Non): ").lower()
    if etat_correct != "oui":
        nouvel_etat = input("Mettre à jour l'état de l'exemplaire. Choisissez parmi [1: Neuf, 2: Abîmé, 3: Bon, 4: Perdu]: ")
        mettre_a_jour_etat(cursor, id_exemplaire, nouvel_etat)
        jours_reste_sanctions = cursor.execute("SELECT nombre_de_jour(idAdherent)")
        print("Vous avez une sanction de %d jours", jours_reste_sanctions)

    date_rendu_prevue = cursor.execute("SELECT dateRenduPrevue FROM Pret")
    date_rendu = datetime.now()
    jours_retard = date_rendu - date_rendu_prevue - 30 
    if jours_retard < 0:
        jours_retard = 0
    if jours_retard > 0:
        printf("Vous avez %s jours de retard, vous allez avoir une sanction", jours_retard)
        jours_reste_sanctions = cursor.execute("SELECT nombre_de_jour(idAdherent)")
        print("Vous avez une sanction de %d jours", jours_reste_sanctions)

    cursor.execute("INSERT INTO Exemplaire empruntable VALUES true")
    print("Retour enregistré. Retour au menu principal.")
    return



#mets a jour l'etat du livre
def mettre_a_jour_etat(cursor, id_exemplaire, nouvel_etat):
    etats = {'1': 'Neuf', '2': 'Abîmé', '3': 'Bon', '4': 'Perdu'}
    etat_selectionne = etats.get(nouvel_etat, None)

    if etat_selectionne is None:
        print("Choix d'état invalide. Aucune mise à jour effectuée.")
        return

    try:
        cursor.execute("UPDATE Exemplaire SET etat = %s WHERE id = %s", (etat_selectionne, id_exemplaire))

        print(f"L'état de l'exemplaire {id_exemplaire} a été mis à jour à '{etat_selectionne}'.")
    except psycopg2.Error as e:
        print("Erreur lors de la mise à jour de l'état :", e)
