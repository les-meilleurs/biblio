import psycopg2
import prettytable
def gerer_personne(cursor):
	print("1. Gérer les adhérents")
	print("2. Gérer le personnel")
	print("3. Quitter")
	choix = input("Choisissez une option : ")
	if choix == '1':
		quitter=False
		while not quitter :
			# Gérer les adhérents
			print("1. Ajouter un adhérent")
		    print("2. Modifier les informations d'un adhérent")
			print("3. Supprimer un adhérent")
		    choix_Action =str(input("Choisissez une option : "))
		    if choix_Action == '1':
				# Ajouter un adhérent
				choix_Personne =str(input("L'adhérent est-il déjà membre du personnel, ou un contributeur d'une ressource? (1-Oui, 2-Non)"))
				if choix_Personne == '1':
					# Si l'adhérent existe déjà dans la base, on demande son id
					id_Personne=str(input("Entrez l'id de la personne :"))
		            ajouter_Adherent_ou_Personnel(cursor, "Adherent")
				elif choix_Personne == '2':
					# Si l'adhérent est une nouvelle personne dans la base,
					# L'id de la nouvelle personne est généré automatiquement et on appelle ajouter_Personne puis ajouter_Adherent
					ok=ajouter_Personne(cursor)
					if ok :
						ajouter_Adherent_ou_Personnel(cursor, "Personnel")
					else :
						print("Ajout non effectué")


	    	elif choix_Action == '2':
				# Modifier les infos d'un adhérent
		        id_Personne=int(input("Entrez l'id de la personne :"))
		        # On affiche les données de l'adhérent
		        print(f"Voici les informations de l'adhérent' '{id_Personne}'")
		        afficher_Adherent(cursor, id_Personne)
				modifier_Adherent_ou_Personnel(cursor, id_Personne, "Adherent")

			elif choix_Action == '3':
				personne_supprimee=False
				while not personne_supprimee :
					# Supprimer un adhérent
		        	id_Personne=int(input("Entrez l'id de la personne :"))
		        	# On affiche les données de la personne
		        	print(f"Voici les informations de la personne {id_Personne}")
		        	ok=afficher_Personne(cursor, id_Personne)
					if not ok :
						print("Id invalide.")
						break
					bonne_personne=int(input("Attention ! Est-ce la bonne personne ? (1-Oui, 0-Non)"))
					if bonne_personne :
						requete=f"DELETE FROM Personne WHERE id_Personne = {id_Personne};"
					    	try:
						        cursor.execute(requete)
							except psycopg2.Error as e:
								# Gérer les erreurs psycopg2
								print("Erreur en faisant la requete suivante : ", e)
								ok=False
							ok=True
						personne_supprimee=ok
					else :
						print("Vérifiez l'id.")

			elif choix_Action == '4':
				# Quitter
				retour=True
	    	else :
	        	print("Attention, choisissez 1, 2, 3 ou 4!")

	if choix == '2':
		quitter=False
		while not quitter :
			# Gérer le personnel
			print("1. Ajouter un membre du personnel")
		    print("2. Modifier les informations d'un membre du personnel")
			print("3. Supprimer un membre du personnel")
		    choix_Action = input("Choisissez une option : ")
		    if choix_Action == '1':
				# Ajouter un adhérent
				choix_Personne = int(input("Le membre est-il déjà un adhérent, ou un contributeur d'une ressource? (1-Oui, 0-Non)"))
				if choix_Personne :
					# Si l'adhérent existe déjà dans la base, on demande son id
					id_Personne=str(input("Entrez l'id de la personne :"))
		            ajouter_Adherent_ou_Personne(cursor, "Personnel")
				else :
					# Si l'adhérent est une nouvelle personne dans la base,
					# On appelle ajouter_Personne puis ajouter_Personel
					ok=ajouter_Personne()
					if ok :
						ajouter_Adherent_ou_Personnel(cursor, "Personnel")
					else :
						print("Ajout non effectué")

	    	elif choix_Action == '2':
				# Modifier les infos d'un adhérent
				id_Personne=int(input("Entrez l'id de la personne :"))
				# On affiche les données de l'adhérent
				print(f"Voici les informations de la personne' '{id_Personne}'")
				infos_Personnel(cursor, id_Personne)
				modifier_Adherent_ou_Personnel(cursor, id_Personne, "Personnel")

			elif choix_Action == '3':
				personne_supprimee=False
				while not personne_supprimee :
					# Supprimer un adhérent
		        	id_Personne=int(input("Entrez l'id de la personne :"))
		        	# On affiche les données de la personne
		        	print(f"Voici les informations de la personne {id_Personne}")
		        	ok=afficher_Personne(cursor, id_Personne)
					if not ok :
						print("Id invalide.")
						break
					bonne_personne=int(input("Attention ! Est-ce la bonne personne ? (1-Oui, 0-Non)"))
					if bonne_personne :
					    sql=f"DELETE FROM Personne WHERE id_Personne = {id_Personne};"
					    	try:
						        cursor.execute(requete)
							except psycopg2.Error as e:
								# Gérer les erreurs psycopg2
								print("Erreur en faisant la requete suivante : ", e)
								ok=False
							ok=True
						personne_supprimee=ok
					else :
						print("Vérifiez l'id.")

			elif choix_Action == '4':
				# Quitter
				quitter=True
	    	else :
	        	print("Attention, choisissez 1, 2, 3 ou 4!")
		elif choix == '3':
			# Quitter
			return
		else :
			print("Attention, choisissez 1, 2 ou 3!")



def ajouter_Adherent_ou_Personnel(cursor, type):
	# On renseigne l'adresse de la personne, son adresse mail, son login, et son mot de passe
	adresse=str(input("Entrez l'adresse de la personne:"))
	adresse_mail=str(input("Entrez l'adresse mail de la personne:"))
	login=str(input("Entrez le login de la personne:"))
	mdp=str(input("Entrez le mot de passe de la personne:"))
	requete = f"INSERT INTO {type} VALUES ('{adresse}', '{adresse_mail}', '{login}', '{mdp}');"
	try:
        cursor.execute(requete)
	except psycopg2.Error as e:
		# Gérer les erreurs psycopg2
		print("Erreur en faisant la requete suivante : ", e)
		return False
	return True

def ajouter_Personne(cursor):
	# On renseigne le nom et le prénom de la personne
	nom=str(input("Entrez le nom de la personne:"))
	return
	prenom=str(input("Entrez le prénom de la personne:"))
	return
	requete = f"INSERT INTO Personne VALUES ('{nom}', '{prenom}');"
	try:
        cursor.execute(requete)
	except psycopg2.Error as e:
		# Gérer les erreurs psycopg2
		print("Erreur en faisant la requete suivante : ", e)
		return False
	return True
	# On renvoie False si une erreur est apparue pendant l'insertion, True sinon

def modifier_Adherent_ou_Personnel(cursor, id, type):
	pas_choix=True
	while pas_choix :
		choix_Modif = input("Quelle information souhaitez-vous modifier ? (adresse, adresse_mail, login, blackliste) ")
		if choix_Modif == 'adresse':
			# Modifier l'adresse
			pas_choix=False
			adresse=str(input("Nouvelle adresse : "))
			requete=(f"UPDATE {type} SET adresse = '{adresse}' WHERE id_Personne = {id};")
			try:
				cursor.execute(requete)
			except psycopg2.Error as e:
				# Gérer les erreurs psycopg2
				print("Erreur en faisant la requete suivante : ", e)
		elif choix_Modif == 'adresse_mail':
			# Modifier l'adresse mail
			pas_choix=False
			adresse_mail=str(input("Nouvelle adresse mail : "))
			requete=(f"UPDATE {type} SET adresse_mail = '{adresse_mail}' WHERE id_Personne = {id};")
			try:
				cursor.execute(requete)
			except psycopg2.Error as e:
				# Gérer les erreurs psycopg2
				print("Erreur en faisant la requete suivante : ", e)
		elif choix_Modif == 'login':
			# Modifier le login
			pas_choix=False
			login=str(input("Nouveau login : "))
			requete=(f"UPDATE {type} SET login = '{login}' WHERE id_Personne = {id};")
			try:
				cursor.execute(requete)
			except psycopg2.Error as e:
				# Gérer les erreurs psycopg2
				print("Erreur en faisant la requete suivante : ", e)
		elif choix_Modif == 'blackliste':
			# Modifier le login
			pas_choix=False
			affiche_sanctions(cursor, id)
			login=str(input("Nouvel etat de blackliste : "))
			requete=(f"UPDATE {type} SET login = '{login}' WHERE id_Personne = {id};")
			try:
				cursor.execute(requete)
			except psycopg2.Error as e:
				# Gérer les erreurs psycopg2
				print("Erreur en faisant la requete suivante : ", e)
		else :
			print("Attention, entrez un type de donnée valide!")

def afficher_Personne(cursor, id_Personne):
	# Affiche les infos d'une personne, renvoie Vrai si pas d'erreurs
    query = f"SELECT nom AS 'Nom', prenom 'Prenom' FROM Personne WHERE id = {id_Personne};"
    try:
        cursor.execute(query)

        # Récupérer la première ligne du résultat
        result = cursor.fetchone()

        # Si aucun genre existe
        if result is None:
            return False
        else:
            # Créer une instance de PrettyTable pour l'exemplaire
            affichage = prettytable.PrettyTable()

            # Ajouter les colonnes et les données à la table en évitant les valeurs None
            for key, value in zip(cursor.description, result):
                if value is not None:
                    affichage.add_column(key[0], [value])

            # Afficher la Personne
			print(affichage)

    except psycopg2.Error as e:
        # Gérer les erreurs psycopg2
        print("Erreur en faisant la requete suivante : ", e)
        return False
	return True

def afficher_Adherent(cursor, id_Personne)
	# Affiche les infos d'un adherent, renvoie Vrai si pas d'erreurs
    query = f"SELECT adresse AS 'Adresse', adresse_mail AS 'Adresse Mail', login AS 'Login', blackliste as 'Est Blackliste' FROM Adherent WHERE id = {id_Personne};"
    try:
        cursor.execute(query)

        # Récupérer la première ligne du résultat
        result = cursor.fetchone()

        # Si aucun genre existe
        if result is None:
            return False
        else:
            # Créer une instance de PrettyTable pour l'exemplaire
            affichage = prettytable.PrettyTable()

            # Ajouter les colonnes et les données à la table en évitant les valeurs None
            for key, value in zip(cursor.description, result):
                if value is not None:
                    affichage.add_column(key[0], [value])

            # Afficher la Personne
			print(affichage)

    except psycopg2.Error as e:
        # Gérer les erreurs psycopg2
        print("Erreur en faisant la requete suivante : ", e)
        return False
	return True

def affiche_sanctions(cursor, id_Personne):
	print(f"Infos de sanctions pour l'adherent {id_Personne}")
	requete=(f"SELECT nombre_de_jour(id_Personne), nombre_de_sanctions(id_Personne);")
	try:
		cursor.execute(query)
		# Récupérer la première ligne du résultat
		result = cursor.fetchone()
		# Si pas bon id
		if result is None:
			print("Id non trouvé.")
			return False
		else:
			# Créer une instance de PrettyTable pour l'exemplaire
			affichage = prettytable.PrettyTable()
			# Ajouter les colonnes et les données à la table en évitant les valeurs None
			for key, value in zip(cursor.description, result):
				if value is not None:
					affichage.add_column(key[0], [value])

			# Afficher les sanctions
			print(affichage)

	except psycopg2.Error as e:
		# Gérer les erreurs psycopg2
		print("Erreur en faisant la requete suivante : ", e)
		return False
	return True
